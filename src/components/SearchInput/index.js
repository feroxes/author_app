import React, {Component} from 'react';

import './SearchInput.css'

class SearchInput extends Component {
    render() {
        const {searchQuery, setSearchQuery} = this.props;
        return (
            <input
                type="text"
                name='search'
                value={searchQuery}
                className='search-input'
                placeholder='Search by author`s name'
                onChange={e => setSearchQuery(e.target.value)}
            />
        );
    }
}

export default SearchInput;
